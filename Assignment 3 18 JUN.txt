﻿ASSIGNMENT - 3 


Ques  :- When we use strictfp?

“strictfp” is a keyword in java which is used as a modifier when we are doing cross platform calculation in order to achieve constant answer on computation of expression containing floating points irrespective of platform(Operating System).

“strictfp” can be written in before the defination of the class, before the written type of concrete methods, with interface definition and with abstract class too.


Ques :- In which case you will use strictfp, "Optimum performance" or "Perfect reproducibility"?

“strictfp” is used when we want the “Perfect reproducibility” as it ensures that output as floating point will be constant irrespective of any operating system, so that the integrity of the data should be maintained. But, by doing so, it creates an additional workload of storing the result of each sub-operation that does not lead to “Optimum performance”


Ques :- In which case you cannot use "strictfp".

    • “strictfp” cannot be used with abstract methods, As abstract methods does not contain any code in it so writing “strictfp” will be worthless.

    • “strictfp” cannot  be written as a modifier with a constructor, and if it is used it will shows illegal modifier used. 

    • “strictfp” cannot be used with variables.


Ques :-  What would be the values of a,b,m and n after execution of these two statements?
int a = 2 * ++m; // now a is 18, m is 8
int b = 2 * n++; // now b is 16, n is 8

In expression 1 :- 
The value of m will get incremented first.
And then it get assigned in a;
Then a = 18 and m = 9
In expression 2 :-
The value of expression gets computed first.
And then the value of n gets incremented.
Then a = 16 and n = 9


Ques :- Why the logical operators "&&" and "||" called "short circuit" operators.

Because it does not required to calculate the result of the second expression/argument, if first argument is sufficient to give the result of the operator (precedence is from left to right).For an instance if the we are using “&&” operator and the result of the first argument is comes out to be true then it will return true without even calculating for the second argument and if false return false, And if we are using “||” operator and if the result of the first operation comes out to be false then only it goes to calculate for the next argument or else return true.


Ques :- Why should we use Math.multiplyExact and other xxExact methods.

xxExact() methods were added in java 8 in order to do all the arithmetic operation with the help of methods.These methods throws Arithmetic Exception if the data overflows in variable of which it is type of.

addExact(int a, int b)
Adds the two parameters, throwing an ArithmeticException in case of overflow (which goes for all *Exact() methods) of the addition:

Math.addExact(100, 50);               // returns 150
Math.addExact(Integer.MAX_VALUE, 1);  // throws ArithmeticException

substractExact()
Substracts the value of the second parameter from the first one, throwing an ArithmeticException in case of overflow of the subtraction:

Math.subtractExact(100, 50);           // returns 50
Math.subtractExact(Long.MIN_VALUE, 1); // throws ArithmeticException

incrementExact()
Increments the parameter by one, throwing an ArithmeticException in case of overflow:

Math.incrementExact(100);               // returns 101
Math.incrementExact(Integer.MAX_VALUE); // throws ArithmeticException

decrementExact()
Decrements the parameter by one, throwing an ArithmeticException in case of overflow:

Math.decrementExact(100);            // returns 99
Math.decrementExact(Long.MIN_VALUE); // throws ArithmeticException

multiplyExact()
Multiply the two parameters, throwing an ArithmeticException in case of overflow of the product:

Math.multiplyExact(100, 5);            // returns 500
Math.multiplyExact(Long.MAX_VALUE, 2); // throws ArithmeticException

negateExact()
Changes the sign of the parameter, throwing an ArithmeticException in case of overflow.

Math.negateExact(100);               // returns -100
Math.negateExact(Integer.MIN_VALUE); // throws ArithmeticException
The second example requires an explanation as it's not obvious: 


Ques :- What is widening?  type casting and what is narrowing type casting.

Widening is the concept of conversion of smaller data into the bigger one. It is an implicit process where we don’t have to specify explicitly  the data type in the expression for the conversion. In the widening process data does not losses.

byte -> short -> char -> int -> long -> float -> double

Narrowing is the concept of conversion of variables of bigger data type into smaller one. In narrowing we have to explicitly specify the data type in which the expression is converting it. In narrowing data loss occurs.

double -> float -> long -> int -> char -> short -> byte

Type casting :- 
It is the process of converting the data type of variable into another type. It can lead to data loss also if we are converting variable of bigger type into smaller one.


Ques :-  What are the four rules that are applied in binary operations on numbers.

Rules for binary addition : -
0 + 0 = 0
0 + 1 = 1
1 + 0 = 1
1 + 1 = 0 and carry 1

Rules for subtraction :-
0 - 0 = 0
0 - 1 = 0 and 1 carry 
1 - 0 = 1 
1 - 1 = 0 

Rules for multiplication :- 
0 * 0 = 0 
1 * 0 = 0 
0 * 1 = 0 
1 * 1 = 1 

Rules for division :- 
0 / 0 = not defined 
1 / 0 = not defined
0 / 1 = 0
1 / 1 = 1 


Ques :- When we use BigInteger and BigDecimal, does mathematical operators such as + and * work on these?
Java BigDecimal class is used to deal with financial data. BigDecimal is preferred while dealing with high-precision arithmetic or situations that require more granular control over rounding off calculations.

BigDecimals are Immutable, arbitrary-precision signed decimal numbers. Java BigDecimal class is defined in the java.math package.

BigInteger class is used for mathematical operations which involve very big integer calculations that are outside the limit of all available primitive data types.

Ques :- What are different ways of declaring an array in Java?

int[] array = new int[size];
		int array1[] =  new int[size];
		int[] array2 = {1,22,4};


Ques :- Why we say that multidimensional arrays in Java are “arrays of arrays”

Multidimensional arrays in java are said to be the arrays of arrays in which the first argument in the size is no. of  reference variable in array and the second argument size describes the size of each array.
Syntax :-
itn[][] array = new int[no. Of  reference variable ][size]


Ques :- What do we mean by “ragged” arrays?

A jagged array, also known as a ragged array or “array of arrays”, is an array whose elements are arrays. The elements of a jagged array can be of different dimensions and sizes, unlike C-style arrays that are always rectangular.

Java has the concept of a distinct size multidimensional array.

itn[][] array = new int[no. Of  reference variable ][];

array[0] = new int[8];
array[1] = new int[23];
array[2] = new int[9];
array[3] = new int[4];

