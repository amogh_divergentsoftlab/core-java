package com.divergent.ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CustomisedException{

	public static void main(String ...arg) {
		
		try {
		Scanner scan = new 	Scanner(System.in);
		System.out.println("Enter your age :- ");
		int age = scan.nextInt();
		
		if (age < 18)
			throw new CustomException("Not valid age to vote");
		
		System.out.println("you are eligible for vote");
		
		}catch(CustomException e) {
			System.out.println(e.getLocalizedMessage());
			
		}catch(InputMismatchException e){
			System.out.println("Please enter numeric value onlyss");
		}
	}
}

class CustomException extends InputMismatchException{

	public CustomException() {
		// TODO Auto-generated constructor stub
	}
	
	public CustomException (String exceptionMessage) {
		super(exceptionMessage);
	}
	
	@Override
	public String getLocalizedMessage() {
		// TODO Auto-generated method stub
		return super.getLocalizedMessage();
	}
}