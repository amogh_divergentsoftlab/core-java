package com.divergent.ExceptionHandling;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class EndOfFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		File file = new File("F:\\fileHandling\\file1.txt");
		
		try {
			if (!file.exists()) {
				throw new IOException("File not found please create on first :");
				//throw new Exception();
			}
			FileReader reader = new FileReader(file);
		
		int wordCount = 0;
		int chr = 0;
		while ((chr = reader.read()) != -1) {
				System.out.print((char)chr);
				wordCount++;
		
		}
		
		if (wordCount != 1024) {
			throw new EOFException("\npromised to have content of 1024 word but that doesn't full fill ");
		}

		
		}catch (FileNotFoundException e) {
			System.out.print("An error occure "+ e.getMessage());
		}
		catch(EOFException e) {
			System.out.println(e.getLocalizedMessage());
		}catch(IOException e) {
			System.out.println("IOExceptionn");
		}
	
	}
}
