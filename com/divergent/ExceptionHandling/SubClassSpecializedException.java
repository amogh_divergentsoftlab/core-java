package com.divergent.ExceptionHandling;

import java.io.FileNotFoundException;

public class SubClassSpecializedException {

	public static void main(String ...arg) {
		
//		SuperClass sc = new DerivedClass();
		try {
			Super sup = new Derived();
			sup.checkExcept();
		} catch (ReflectiveOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

class Super{
	
	 void checkExcept()  throws ReflectiveOperationException{
		System.out.println("Super class method");
	}
}

class Derived extends Super{
	
	 void checkExcept() throws ClassNotFoundException{
		System.out.println("Derived class method");
		Class.forName("com.mysql.cj.jdbc.Driver");
	}
}