package com.divergent.JavaDoc.src;

/**
 * 
 * @author Amogh bhoj 
 * @version 0.0.1
 * @since 1.5
 *
 */
public class Test{
	
	/**
	 * Represents the minimum marks that can be obtained form the test.
	 */
	private int minMarks;
	
	/**
	 * Represents the maximum marks that can be obtained from the test.
	 */
	private int maxMarks;

	/**
	 * 
	 * @return the minimum marks obtained by the student.
	 */
	public int getMinMarks() {
		return minMarks;
	}

	/**
	 * @param minMarks obtained by the student.
	 */
	public void setMinMarks(int minMarks) {
		this.minMarks = minMarks;
	}

	/**
	 * @return maximum marks obtained by the student.
	 */
	public int getMaxMarks() {
		return maxMarks;
	}

	/**
	 * @param maxMarks obtained by the student.
	 */
	public void setMaxMarks(int maxMarks) {
		this.maxMarks = maxMarks;
	}
	
}
