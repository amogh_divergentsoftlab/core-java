package com.divergent.amoghbhoj;

public class BinaryOperation {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int i = 0b1010;
		int j = 0b1100;
		
		/*
		 *  here in additon of i and j be like :-
		 *  
		 *  	1 0 1 0 
		 *  +   1 1 0 0 
		 *  	_______ 
		 *    1 0 1 1 0
		 *    
		 *     where ,
		 *     0 + 0 = 0
		 *     1 + 0 = 1 
		 *     0 + 1 = 1
		 *     1 + 1 = 0 and 1 carry
		 */

		System.out.println(i + j);
	}
	
}
