package com.divergent.amoghbhoj;

class Constructor{
	 
	int cons;
	
	public Constructor(int cons) {
		System.out.println("constructor with arguments :");
	}
	
	public Constructor() { }
}
public class ConsConcept {
	
	public static void main(String args[]) {
		
	Constructor cons = new Constructor();
	
	}
	/*
	 * if we do not create any constructor in the class then jvm internally create no argument constructor.
	 * But if we create a parameterized constructor and then call for no argument constructor then it will give compilation problem 
	 * constructor not found cause in that case jvm not create any no argument constructor implicitly and we have to create it explicitly 
	 * for our own need. 
	 */
}