package com.divergent.amoghbhoj;

class ConsChain{
	
	int i , j;
	ConsChain() {System.out.println("this is written inside no argument constructor ");}
	
	ConsChain(int i) {this(); System.out.println("this is written inside one argument constructor");}
	
	ConsChain(int i , int j) { this(3); System.out.println("this is written inside two argument constructor");}
	
}
public class ConstrucorChain {
	
	public static void main(String args[]) {
		
		ConsChain con1 = new ConsChain(5);
	}
}
