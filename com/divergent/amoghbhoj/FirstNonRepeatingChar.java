package com.divergent.amoghbhoj;

public class FirstNonRepeatingChar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str0 = "Amogham";
		String str = str0.toLowerCase();
		boolean duplicate = false;
		
		for(int i = 0; i< str.length(); ++i) {
			for(int j=i+1; j <str.length(); ++j) {
				
				if (str.charAt(i) == str.charAt(j)) {
					duplicate = true;
					break;
				}
				
			}
			
			if (duplicate == false) {
				System.out.println(str.charAt(i) + " Is the first non repeating character in the string.");
				break;
			}
			duplicate = false;
			
		}
		
	}

}
