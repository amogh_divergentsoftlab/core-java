package com.divergent.amoghbhoj;

class Addition{
	
	void add(int i, int j) {
		System.out.println("add method using two arguments : " + (i + j));
	}
	
	void add(int i, int j, int k) {
		System.out.println("add method using three arguments : " + (i + j + k));
	}
	
	void add(int i, int j, int k, int f) {
		System.out.println("add method using four arguments : " + (i + j + k + f));
	}
	
	void add(int i, int j, int k,int f, int l) {
		System.out.println("add method using five arguments : " + (i + j + k + f + l));
	}
}
public class MethodOverloading {
	
	public static void main(String args[]) {
		
		Addition add = new Addition();
		
		add.add(2, 3);
		add.add(2, 3,6);
		add.add(2, 3, 6, 7);
		add.add(2, 3, 6, 7, 9);
	}
}
