package com.divergent.amoghbhoj;

//Write a program to prove that you can avoid NullPointerException using "&&" operator.

public class NullException {
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "hello world";
		
		if(str == null && nullCheck() ){
			System.out.println("NullPointerException");
			System.exit(1);
		}
		
		System.out.println("Program runs successfully");
		
		
	}
	
	public static boolean nullCheck() {
		return true;
	}

}
