package com.divergent.amoghbhoj;

class ReferenceClass{
	
	StringBuilder ref;
	
	void useRef() {
		ref = new StringBuilder("current refference.");
		System.out.println("ref value : " + ref); // current reference 
		this.changeRef(ref);
		System.out.println("after changed ref : " + ref); // current reference
	}

	private void changeRef(StringBuilder ref2) {
		ref2 = new StringBuilder("changed refence.");
		System.out.println("inside change ref : " + ref2); // changed reference
	}
}
public class Reference {
	    // Driver method
	public static void main(String args[]) {
	
		ReferenceClass ref = new ReferenceClass();
		ref.useRef();
	}
}
