package com.divergent.amoghbhoj;

public class RepeatingCharacter{

	public static void main(String[] args) {
		
		String str0 = "Amogha";
		String str = str0.toLowerCase();
		int duplicate = 0;
	
		for(int i = 0; i< str.length(); ++i) {
			for(int j=i+1; j <str.length(); ++j) {
				if (str.charAt(i) == str.charAt(j)) {
					duplicate++;
					break;
				}
				
			}
			
		}
		
		System.out.println("The no. of duplicate character in string are :- "+  duplicate);
	}

}
