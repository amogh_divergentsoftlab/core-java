package com.divergent.amoghbhoj;

public class ReverseString{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "hello world";
		byte[] array = new byte[str.length()];
		
		int j =0;
		for(int i = str.length()-1 ; i >= 0 ; i--) {
			
			array[j] = (byte) str.charAt(i);
			j++;
		}
		
		String reverse = new String(array);
		
		System.out.println(reverse);
		
	}

}