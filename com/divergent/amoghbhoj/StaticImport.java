package com.divergent.amoghbhoj;

public class StaticImport{
	
	public static void main(String args[]) {
		
		System.out.println(Math.addExact(2, 2));
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Float.MIN_VALUE);
		
		/*
		 *  Static import are used to get teh static functionalities and the field of that particular class.
		 */
	}
	
}
