package com.divergent.amoghbhoj;

public class VowelCount {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "Amogh".toLowerCase();
		int vCount = 0;
		
		for (int i = 0; i < str.length(); i++ ) {
			
			char ch = str.charAt(i);
			
			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
				vCount++;
			}
			
		}
		
		System.out.println("The no. of vowels im string are : " + vCount);
		System.out.println("The no. of consonant in string are : " + (str.length() - vCount));
		
	}

}
