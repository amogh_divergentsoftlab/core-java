package com.divergent.inheritance;

public class ConstructorChaining {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Third third = new Third();
		
	}

}

class Super{
	
	public Super() {
		// TODO Auto-generated constructor stub
		System.out.println("hello from super class constructor :");
	}
	
}

class Derived extends Super{
	
	public Derived() {
		// TODO Auto-generated constructor stub
		System.out.println("hello from derived class constructor :");
	}
}

class Third extends Derived{
	
	public Third() {
		// TODO Auto-generated constructor stub
		System.out.println("hello from third class constructor :");
	}
	
}
