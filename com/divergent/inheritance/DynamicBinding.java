package com.divergent.inheritance;

public class DynamicBinding {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employees employee = new Employees();
		System.out.println("Employee get salary of : " + employee.getSalary());
		
		Managers manager = new Managers();
		System.out.println("Manager get salary of : " + manager.getSalary());
		
		var Employee = new Managers();
		System.out.println("Employee get salary of : " + employee.getSalary());

	}

}

class Employees{
	
	int salary = 100000;
	
	public int getSalary() {
		return salary;
	}
	
}

class Managers extends Employees{
	
	int bonus = 10000;
	
	public int getSalary() {
		return super.salary + bonus; 
	}
}