package com.divergent.inheritance;


/*
 * 2 . What do we mean when we say that "most general methods in the superclass and more 
	specialized methods in its subclasses", explain with a program?
 */
public class GeneralizedAndSpecilaizedExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Men men = new Men();
		
		men.cabTalk();
		System.out.println("men live : " + men.lives);
		System.out.println("men have hard voice : " + men.hardVoice);
		men.areStrong();
		
		System.out.println("-------------");
		Women women = new Women();
		
		women.cabTalk();
		System.out.println("men live : " + women.lives);
		System.out.println("men have hard voice : " + women.softVoice);
		women.areBeautiful();
	}

}

class Human {
	
	public boolean lives = true;
	
	public void cabTalk() {
		System.out.println("Yes human can talk ");
	}
}

class Men extends Human{
	
	public boolean hardVoice = true; 
	
	public void areStrong() {
		System.out.println("Yes men are storng");
	}
}

class Women extends Human{
	
	public boolean softVoice = true;
	
	public void areBeautiful() {
		System.out.println("yes women are naturally beautiful ");
	}
}
