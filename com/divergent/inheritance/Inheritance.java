package com.divergent.inheritance;

/*
 * Inheritance example using Employee and Manger as class 
 * as Employee class has generalized fields and method and 
 * Manger has specific field and methods.
 * 
 */
public class Inheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Manager manager = new Manager();
		System.out.println(manager.getSalary());
	}

}

class Employee{
	
	public int salary;
	
	public Employee() {
		// TODO Auto-generated constructor stub
		this.salary = 10000;
				
	}
	
	public void work() {
		System.out.println("Employee work");
	}
}

class Manager extends Employee{
	
	int bonus = 1000;
	
	int getSalary() {
			
		return this.salary + bonus; 
	}
	
}
