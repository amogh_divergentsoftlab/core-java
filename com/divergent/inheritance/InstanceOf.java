package com.divergent.inheritance;

public class InstanceOf {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Animal animal = new Monkey();
		
		if ( animal instanceof Monkey) {
			System.out.println("animal is an instance of monkey");
		}
		
		Monkey monkey = new Monkey();
		
		if(monkey instanceof Animal)
			System.out.println("monkey is an instance of Animal");
	}

}

class Animal{
	
	public void eat() {
		System.out.println("animals can eat there food :");
	}
}

class Monkey extends Animal{
	
	public void eat() {
		System.out.println("monkeys can eat there food with hands :");
	}
}