package com.divergent.inheritance;


/*
 * 3. What is the method overriding, explain with a program?
 */
public class MethodOverriding {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		SportsCar sportsCar = new SportsCar();
		sportsCar.move();
		
	}

}

class Vehicle{
	
	void move() {
		System.out.println("vehicles moves");
	}
}

class SportsCar extends Vehicle{

	@Override
	void move() {
		System.out.println("sports car moves at high speed");
	}
}