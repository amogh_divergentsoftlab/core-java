package com.divergent.inheritance;

public class Polymorphism {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Parent parent = new Child();
		
		parent.height();
	}

}

class Parent{
	
	public void height () {
		System.out.println("Parents are tall in height");
	}
	
}

class Child extends Parent{
	
	public void height() {
		System.out.println("Chid has medium height ");
	}
	
}