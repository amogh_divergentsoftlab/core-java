package com.divergent.inheritance;

/*
 * Since super is a keyword it can not be initialized in any type of variable
 * it is only use to use parent class fields and methods and constructor as well, 
 * but in constructor first statement must be super.
 */
public class A 
	public void m1() {
		A a = this ;
	}
}

class B extends A {
	public void m2() {
		A a = super ; // this will give error since super is not a reference
	}
}