package com.divergent.inheritance;

public class SuperClassConstructor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Derived derived = new Derived();
	}

}

class Super{
	
	public Super(int i) {
		// TODO Auto-generated constructor stub
		System.out.println("hello from super class constructor :");
	}
	
	public Super() {}
}

class Derived extends Super{
	
	public Derived() {
		// TODO Auto-generated constructor stub
		System.out.println("hello from derived class constructor :");
	}
}