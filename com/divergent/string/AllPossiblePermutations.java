package com.divergent.string;

public class AllPossiblePermutations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "abcd";
		calculatepermutation(str,0,str.length()-1);
		
	}

	private static void calculatepermutation(String str, int i, int j) {
		// TODO Auto-generated method stub
		
		if(i == j) 
			System.out.print(str + " ");
		else {
			 for (int k = i; k <= j ; k++) {
				
				 str = swap(str,i,k);
				 calculatepermutation(str, i+1, j);
				 str = swap(str,i,k);
			 }
			
		}
		
	}

	private static String swap(String str, int k, int i) {
		// TODO Auto-generated method stub
		
		char[] array = str.toCharArray();
		char temp = array[i];
		array[i] = array[k];
		array[k] = temp;
		
		return String.valueOf(array);
	}

}
