package com.divergent.string;

public class AnagramsStrings {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str1 = "silent";
		String str2 = "listen";
		
		int points = 0;
		
		if(str1.length() != str2.length()) {
			System.out.println("Strings are not anagrams ");
			return;
		}
		
		
		for (int i = 0; i < str1.length(); i++) {
			
			for (int j = 0; j < str1.length(); j++) {
				
				if(str1.charAt(i) == str2.charAt(j)) {
					points++;
				}
			}
		}
		
		if (points >= str1.length())
			System.out.println("Strings are anagrams");
		else 
			System.out.println("Strings are not annagrams");
	}

}
