package com.divergent.string;

import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyFormatter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int million = 1000000;
		
		 NumberFormat formatter=NumberFormat.getCurrencyInstance(Locale.US); 
		 NumberFormat formatter1 = NumberFormat.getCurrencyInstance(Locale.ITALIAN);
		 
		 String currencyUS =formatter.format(million);  
		 String currercyIT = formatter1.format(million);
		 
		 System.out.println(currencyUS + " for the US");
		 System.out.println(currercyIT + " for itailian");
	}
}
