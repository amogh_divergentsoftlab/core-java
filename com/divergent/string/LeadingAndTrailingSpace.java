package com.divergent.string;

public class LeadingAndTrailingSpace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str  = "   Amogh Bhoj is my Name          ";
		System.out.println(str.length());
		while(true) {
			
			if ((int)str.charAt(0) == 32)
				str = str.substring(1, str.length());
			
			if ((int)str.charAt(str.length()-1) == 32)
				str = str.substring(0,str.length()-1);
			
			if ((int)str.charAt(0) != 32 && (int)str.charAt(str.length()-1) != 32) {
				break;
			}
		}
		
		System.out.println(str);
		System.out.println(str.length());

	}

}
