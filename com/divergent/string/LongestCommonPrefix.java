package com.divergent.string;

public class LongestCommonPrefix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "Amogh Bhoj";
		String str2 = "Amogjh";
		int prefixLenght = 0;
		
		int length = str.length() < str2.length()? str.length(): str2.length();
		
		for (int i = 0; i < length; i++) {
			
			if (str.charAt(i) == str2.charAt(i)) {
				prefixLenght++;
			}else {
				break;
			}
			
		}
		
		if(prefixLenght == 0) {
			System.out.println("no common prefix found for given two strings.");
		}else {
			System.out.println("common prefix found of length : " + prefixLenght);
		}
	}

}
