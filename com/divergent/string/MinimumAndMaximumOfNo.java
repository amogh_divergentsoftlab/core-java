package com.divergent.string;

public class MinimumAndMaximumOfNo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num1 = 89;
		int num2 = 780;
		
		int minimum = min(num1,num2);

		if(minimum == num1) {
			System.out.println(num1 + " is minimum");
			System.out.println(num2 + " is maximum");
		}else {
			System.out.println(num2 + " is minimum");
			System.out.println(num1 + " is maximum");
		}
	}

	private static int min(int num1, int num2) {
		// TODO Auto-generated method stub
		
		return (num1 < num2)? num1:num2;
	}

	
}
