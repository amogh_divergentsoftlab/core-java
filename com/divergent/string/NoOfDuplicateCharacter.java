package com.divergent.string;

public class NoOfDuplicateCharacter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "Amit sigh chauhan".toLowerCase();
		
		int length = str.length();
		int duplicateCount = 0;
		
		for (int i = 0; i< str.length(); i++) {
			for(int j = i +1; j < str.length(); j++) {
				
				if (str.charAt(i) == str.charAt(j))
					++duplicateCount;
			}
			
			System.out.println("for char " + str.charAt(i) + " " + i);
			System.out.println(duplicateCount );
		}
		
		System.out.println("The no. of duplicate charater in the string is : " + duplicateCount);
	}

}
