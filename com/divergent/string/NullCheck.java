package com.divergent.string;

public class NullCheck {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = null;
		
		if (str != null && str.length() == 0) {
			System.out.println("str is not null");
		}
		
		if (str.length() == 0 && str == null) {
			System.out.println("string with zero size");
		}
	}

}
