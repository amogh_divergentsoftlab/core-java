package com.divergent.string;

public class OnlyDigit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 String test = "7987987979";
		 int length = test.length();
		 
		 boolean flag = false;
		 
		 
		 for (int i = 0; i < length; i++) {
			 
			 int code = (int)test.charAt(i);
			 if (code > 57 || code < 48) {
				 flag = true;
				 break;
			 }
			 
		 }
		 
		 if (flag == true)
			 System.out.println("alphabets exists in the given string");
		 else
			 System.out.println("only numeric value exists in the string");
	}

}
