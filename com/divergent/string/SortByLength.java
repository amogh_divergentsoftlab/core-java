package com.divergent.string;

import java.util.Arrays;

public class SortByLength {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String array[] = {"amogh","amit","akash","adarsh","priyanshi","arti"};
		
		for(int i = 0; i < array.length ; i++) {
			
			for(int j = i + 1; j < array.length ; j++) {
				
				 if(array[i].length() > array[j].length()) {
					
					 String temp = array[i];
					 array[i] = array[j];
					 array[j] = temp;
					 
				 }
			}
		}
		
		System.out.println(Arrays.toString(array));
		
	}

}
