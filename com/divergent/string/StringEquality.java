package com.divergent.string;

public class StringEquality {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "Amogh";
		String str1 = "Amogh";
		
		String str2 = new String("Amogh");
		String str3 = new String("Amogh");
		
		
		System.out.println(str == str1);
		System.out.println(str.equals(str1));
		
		
		System.out.println(str2 == str3);
		System.out.println(str2.equals(str3));
		
		System.out.println(str == str2);
		System.out.println(str.equals(str3));
		System.out.println(str2.equals(str1));
		
	}

}
