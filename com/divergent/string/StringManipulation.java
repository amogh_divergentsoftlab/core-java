package com.divergent.string;

import java.lang.reflect.Field;

public class StringManipulation {

	public static void main(String[] args) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		// TODO Auto-generated method stub
		
		String str = "Amogh bhoj";
		Field field = String.class.getDeclaredField("value");
		field.setAccessible(true);
		byte array[] = (byte[])field.get(str);
		
		array[0] = 51;
		array[1] = 57;
		
		System.out.println(str);
	}

}
