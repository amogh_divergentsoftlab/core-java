package com.divergent.string;

public class SubStringExists {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "Amogh Bhoj";
		String sub = "og";
		boolean flag = false	;
		
		
		for(int i =0; i < str.length(); i++) {
			
			if (str.charAt(i) == sub.charAt(0)) {
				flag = true;
				
				for (int j = 1 ; j < sub.length(); j++) {
					if(str.charAt(i +j) != sub.charAt(j)) {
							flag = false; break;
					}else
						flag = true;
					
				}
				
				if (flag == true) {
					System.out.println("substring found in the string");
					System.exit(1);
				}
				
			}
		}
		
		System.out.println("substring not found");
	}

}
