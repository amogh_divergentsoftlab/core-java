package com.divergent.string;

public class SuperKeyword {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Child child = new Child(1,2);
	}

}


class Parent{
	
	public Parent(){
		System.out.println("this is no args cons in Parent class");
	}
	
	public Parent(int i, int a){
		System.out.println("this is  args cons in Parent class");
	}
	
}


class Child extends Parent{
	
	public Child() {
		super(7,8);
		// TODO Auto-generated constructor stub
		System.out.println("Child class no args cons ");
	}
	
	public Child(int i, int a) {
		
		System.out.println("This is args cons in child class");
	}
}