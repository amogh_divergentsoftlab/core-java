package com.divergent.string;

import java.util.Scanner;

public class ThreadSafe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		thread();
	}

	private static void thread() {
		// TODO Auto-generated method stub
		
		T t = new T();
		
		Thread thread = new Thread(t);
		Thread thread1 = new Thread(t);
				
		thread.start();
		thread1.start();
		
//		T t = new T();
//		Ti ti = new Ti();
//		
//		t.start();
//		ti.start();
	
	}

}

//class Ti extends Thread{
//
//	@Override
//	public void run() {
//		// TODO Auto-generated method stub
//	
//		for (int i = 0;i < 12 ; i ++) {
//			//StringBuilder str = new StringBuilder("Hello from Amogh in Ti");
//			StringBuffer str = new StringBuffer("Hello from Amogh in Ti");
//			System.out.println(str);
//		}
//	}
//	
//	
//}
//
class T implements Runnable{

	@Override
	public void run() {
		// TODO Auto-generated method stub
	
		for (int i = 0;i < 12 ; i ++) {
			StringBuffer str = new StringBuffer("Hello from Amogh in T");
			System.out.println(str);
		}
	}
	
	
}