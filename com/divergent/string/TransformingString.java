package com.divergent.string;

public class TransformingString {

	public static void main(String ...agrs) {
		
		String str = "abc";
		String str2 = "bed";
		
		char[] array1 = str.toCharArray();
		char[] array2 = str2.toCharArray();
		
		int length = str.length();


		for(int i = 0; i < length; i++) {
			
			if( str.charAt(i) != str2.charAt(i)) {
				char ch = str.charAt(i);
				boolean found = false;  

				for(int j = i+1; j < str2.length(); j++) {
					
					
					if (ch == str2.charAt(j)) {
						
						found = true;
	
						char temp = array2[j];
						array2[j] = array2[i];
						array2[i] = temp;
				
					}
					
				}
				
				if (!found) {
					array2[i] = array1[i];
				}
				
			}
			
		}
		
		str2 = new String(array2);
		
		System.out.println(str);
		System.out.println(str2);
		
	}
}
